﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;

namespace HaxBallServer.Server.Hubs
{
	public class GameHub : Hub
	{
		private List<string> _gameRooms = new List<string>();
		private readonly ConcurrentDictionary<string, HashSet<string>> _groupMembers = new ConcurrentDictionary<string, HashSet<string>>();

		public async Task JoinRoom(string roomName)
		{
			await Groups.AddToGroupAsync(Context.ConnectionId, roomName);
			await Clients.Caller.SendAsync("AddMessage", Context.ConnectionId + " welcome to room " + roomName);
			await Clients.OthersInGroup(roomName).SendAsync("AddMessage", Context.ConnectionId + " welcome to room " + roomName);
			await Clients.Group(roomName).SendAsync("UserJoinedRoom", Context.ConnectionId);
			_gameRooms.Add(roomName);
			Console.WriteLine("JOINED");
			_groupMembers.AddOrUpdate(roomName,
				key => new HashSet<string> { Context.ConnectionId },
				(key, value) => { value.Add(Context.ConnectionId); return value; });
		}

		public async Task LeaveRoom(string roomName)
		{
			await Groups.RemoveFromGroupAsync(Context.ConnectionId, roomName);
			_groupMembers.AddOrUpdate(
				roomName,
				key => new HashSet<string>(),
				(key, value) => { value.Remove(Context.ConnectionId); return value; });
		}

		public Task GetUsersInRoom(string roomName)
		{
			if (_groupMembers.TryGetValue(roomName, out var userIds))
			{
				return Clients.Caller.SendAsync("ReceiveUsersInRoom", userIds);
			}

			return Task.CompletedTask;
		}

		public async Task SendRooms()
		{
			Console.WriteLine(_gameRooms.Count + " ABC");
			await Clients.Caller.SendAsync("GetRooms", _gameRooms);
		}

		public async Task SendTransformation(float x, float y)
		{
			Console.WriteLine("x " + x + ", y " + y);
			await Clients.Others.SendAsync("GetTransformation", x, y);
		}
		public async Task SendMessage(string user, string message)
		{
			await Clients.All.SendAsync("ReceiveMessage", user, message);
		}

		public async Task UpdateScore(string user, int score)
		{
			await Clients.All.SendAsync("ReceiveScoreUpdate", user, score);
		}
	}
}
